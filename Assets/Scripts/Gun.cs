using UnityEngine;

[CreateAssetMenu(fileName = "New Gun", menuName = "Gun")]
public class Gun : ScriptableObject
{
    public int Damage;

    public int GunCartridge;

    public int TotalCapacityGun;

    public string NameGun;

    public string DescriptionGun;

    public Sprite ImageGun;

    public GameObject PrefabGun;

    public GameObject PrefabBullet;
}
