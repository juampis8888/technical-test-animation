using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class InfoAdapter : MonoBehaviour
{
    public Image SpriteGun;

    public Button ButtonExit;

    public Button ButtonSelect;

    public TextMeshProUGUI Description;

    public TextMeshProUGUI NameGun;

    public TextMeshProUGUI Damage;

    public TextMeshProUGUI GunCartridge;

    public TextMeshProUGUI TotalCapacityGun;

    public void Parent(Transform Parent)
    {
        transform.SetParent(Parent);
    }

    public void Location(float topX, float topY)
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        rectTransform.localPosition = new Vector3(topX, topY, 0);
    }
}
