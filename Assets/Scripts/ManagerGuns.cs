using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ManagerGuns : MonoBehaviour
{
    public GameObject Player;

    public List<Gun> ModelGun;

    public Vector3[] SpawnGuns;

    public string GunCurrent;

    public int magnetDistance;

    public float SpeedObjects;
    
    public float ProjectileVelocity;

    private float FlightDuration = 20;

    private GameObject GameObjectCurrentGun;
    void Start()
    {
        for(int i = 0; i < ModelGun.Count; i++)
        {
            var obj = Instantiate(ModelGun[i].PrefabGun,SpawnGuns[i], ModelGun[i].PrefabGun.transform.rotation);
        } 
        
    }

    // Update is called once per frame
    void Update()
    {
        
         if (Input.GetMouseButtonDown(1) & GunCurrent == "GunOne")
         {
             StartCoroutine(BulletParabolicMovement(SearchGun(GunCurrent)));
         }

         if(Input.GetMouseButtonDown(1) & GunCurrent == "GunTwo")
         {
             StartCoroutine(BulletMovement(SearchGun(GunCurrent)));
         }

         if (Input.GetMouseButtonDown(1) & GunCurrent == "GunThree")
         {
             StartCoroutine(BulletRepulsiveMovement(SearchGun(GunCurrent)));
         }
        
    }

    public Gun SearchGun(string Name)
    {
        return ModelGun.Find(gun => gun.NameGun == Name);
    }

    IEnumerator BulletParabolicMovement(Gun gun)
    {
        yield return new WaitForSeconds(0f);

        var objtBullet = Instantiate(gun.PrefabBullet);
        objtBullet.transform.localPosition = GameObjectCurrentGun.transform.GetChild(0).transform.position;
        objtBullet.transform.localRotation = GameObjectCurrentGun.transform.parent.rotation;

        float Vx = Mathf.Sqrt(ProjectileVelocity) * Mathf.Cos(transform.rotation.x * Mathf.Deg2Rad);
        float Vy = Mathf.Sqrt(ProjectileVelocity) * Mathf.Sin(transform.rotation.x * Mathf.Deg2Rad);


        float ElapseTime = 0;

        while (ElapseTime < FlightDuration)
        {

            objtBullet.transform.Translate(0, (Vy - ElapseTime) * Time.deltaTime, Vx * Time.deltaTime);

            ElapseTime += Time.deltaTime;
            
            yield return null;
        }
    }

   IEnumerator BulletMovement(Gun gun)
    {
        yield return new WaitForSeconds(0f);

        var objtBullet = Instantiate(gun.PrefabBullet);
        objtBullet.transform.localPosition = GameObjectCurrentGun.transform.GetChild(0).transform.position;
        objtBullet.transform.localRotation = GameObjectCurrentGun.transform.parent.rotation;


        while (true)
        {
            Collider[] hitColliders = Physics.OverlapSphere(objtBullet.transform.position, magnetDistance);

            for (int i = 0; i < hitColliders.Length; i++)
            {
                if (!hitColliders[i].tag.Contains("Gun") & !hitColliders[i].tag.Contains("Player") & !hitColliders[i].tag.Contains("Ground") & !hitColliders[i].tag.Contains("Bullet"))
                {
                    Transform obj = hitColliders[i].transform;
                    Debug.Log(Vector3.Distance(objtBullet.transform.position, obj.position));
                    if(Vector3.Distance(objtBullet.transform.position,obj.position) >= 5) 
                    {
                        obj.position = Vector3.MoveTowards(obj.position, objtBullet.transform.position, SpeedObjects * Time.deltaTime);

                    }else if(Vector3.Distance(obj.position, objtBullet.transform.position) < 5)
                    {
                        obj.SetParent(objtBullet.transform);
                        obj.RotateAround(objtBullet.transform.position, Vector3.up, 90 * Time.deltaTime);
                        Debug.Log("Rotate");
                    }
                }
            }

            objtBullet.transform.Translate(Vector3.forward * ProjectileVelocity* Time.deltaTime);


            //ElapseTime += Time.deltaTime;

            yield return new WaitForSeconds(0f);
        }
    }


    IEnumerator BulletRepulsiveMovement(Gun gun)
    {
        yield return new WaitForSeconds(0f);

        var objtBullet = Instantiate(gun.PrefabBullet);
        objtBullet.transform.localPosition = GameObjectCurrentGun.transform.GetChild(0).transform.position;
        objtBullet.transform.localRotation = GameObjectCurrentGun.transform.parent.rotation;

        while (true)
        {
            Collider[] hitColliders = Physics.OverlapSphere(objtBullet.transform.position, magnetDistance);

            for (int i = 0; i < hitColliders.Length; i++)
            {
                if (!hitColliders[i].tag.Contains("Gun") & !hitColliders[i].tag.Contains("Player") & !hitColliders[i].tag.Contains("Ground") & !hitColliders[i].tag.Contains("Bullet"))
                {   
                    Debug.Log(hitColliders[i].tag);
                    Transform obj = hitColliders[i].transform;
                    if(obj.position.x > 0f)
                    {
                        obj.Translate(new Vector3(1,0,0) * ProjectileVelocity * Time.deltaTime);
                    }
                    else if(obj.position.x < 0f)
                    { 
                        obj.Translate(Vector3.left * ProjectileVelocity * Time.deltaTime);
                    }
                }
            }
            objtBullet.transform.Translate(Vector3.forward * ProjectileVelocity * Time.deltaTime);

            yield return null;
        }
    }



    public void SetGun(GameObject gun)
    {
        GameObjectCurrentGun = gun;
    }
}

