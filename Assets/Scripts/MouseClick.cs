using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseClick : MonoBehaviour
{   
    private ShowInfo ShowInfo;

    void Start()
    {
        ShowInfo = GameObject.FindGameObjectWithTag("Show Info").GetComponent<ShowInfo>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnMouseDown()
    {   
        if (ShowInfo.Content.childCount == 0)
        {
            ShowInfo.Info(gameObject.tag);
        }
    }
}
